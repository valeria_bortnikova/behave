from selenium import webdriver
import time
from selenium.webdriver.common.by import By


LINK = 'https://yandex.ru/'


def test_selenium():
    options = webdriver.ChromeOptions()
    capabilities = {
        "browserName": "chrome",
        "browserVersion": "96.0",
        "selenoid:options": {
            "enableVNC": True,
            "enableVideo": False
        }
    }

    browser_remote = webdriver.Remote(
        command_executor='http://aft.onriva.com:4444/wd/hub',
        options=options,
        desired_capabilities=capabilities,
    )
    browser_local = webdriver.Chrome()

    browser = browser_remote

    with browser:
        browser.get(LINK)
        button = browser.find_element(By.XPATH, "//div[contains(text(), 'Переводчик')]")
        button.click()
        time.sleep(3)
        browser.switch_to.window(browser.window_handles[1])
        print(browser.title)


if __name__ == '__main__':
    test_selenium()