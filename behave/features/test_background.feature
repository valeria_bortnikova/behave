Feature: checking if the flights are available

  Background: page with flights is opened
    Given customer is logged in
      And customer opened page with flights

  @flights
  Scenario: flights are available for a customer
    Given customer is on the page with flights
      When customer filled all the fields
      And click the button "Search"
    Then some flights are available