Feature: available countries

  Scenario: available countries with capitals
    Given several countries
      | country | capital |
      | Russia  | Moscow  |
      | Belarus | Minsk   |

    When we choose the country "Russia"
    Then the capital should be "Moscow"