from behave import *


@given('password field is active')
def step_impl(context):
    print('step Given passed')


@when('we create password')
def step_impl(context):
    print('step When passed')


@then('the password should follow the rules')
def step_impl(context):
    print('step Then passed with context.text')