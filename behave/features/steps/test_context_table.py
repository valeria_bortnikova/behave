from behave import *


@given('several countries')
def step_impl(context):
    print('step Given passed with context.table')


@when('we choose the country "Russia"')
def step_impl(context):
    print('step When passed')


@then('the capital should be "Moscow"')
def step_impl(context):
    print('step Then passed')