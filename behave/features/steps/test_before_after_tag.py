from behave import *


@given('title page is opened')
def step_impl(context):
    print('step Given passed')


@when('I look at the title page')
def step_impl(context):
    print('step When passed')


@then('I can see company logo in the header')
def step_impl(context):
    print('step Then passed')
