from behave import *


@given('customer is logged in')
def step_impl(context):
    print('step Given from background passed')


@given('customer opened page with flights')
def step_impl(context):
    print('step And from background passed')


@given('customer is on the page with flights')
def step_impl(context):
    print('step Given from scenario passed')


@when('customer filled all the fields')
def step_impl(context):
    print('step When passed')


@when('click the button "Search"')
def step_impl(context):
    print('step And passed')


@then('some flights are available')
def step_impl(context):
    print('step Then passed')