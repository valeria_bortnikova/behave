from behave import *


@given('login page is opened')
def step_impl(context):
    print('step 1 passed')


@when('we fill <login> and <password>')
def step_impl(context):
    print('step 2 passed')


@when('click the button LogIn')
def step_impl(context):
    print('step 3 passed')


@then('we sign in')
def step_impl(context):
    print('step 4 passed')