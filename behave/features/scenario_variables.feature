Feature: log in

  Scenario Outline: Login parameters
    Given login page is opened
      When we fill valid <login> and <password>
      And click the button LogIn
      Then we sign in

    Examples: Valid users
      | login | password |
      | Ivan  | 1234     |
      | Maria | 0000     |

    Examples: Invalid users
      | login | password |
      | Den   | aaaa     |
      | Luca  | -        |

