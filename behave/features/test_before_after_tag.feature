Feature: company logo is in the header

  @logo
  Scenario: logo is visible
    Given title page is opened
    When I look at the title page
    Then I can see company logo in the header
